jQuery(document).ready(function(){
  //
  // parallax
  //
  if(!device.mobile() && !device.tablet() && !$('html').hasClass('ie')) {
    if($('[data-stellar-background-ratio]').length) {
      $.stellar();
    }
  }
  //
  // scroll to bottom
  //
  $('.homepage-slider__arrow').on('click', function(){
    var $scroll_el = $(this).attr('href');
    if ($($scroll_el).length) {
      $('html, body').animate({
        scrollTop: $($scroll_el).offset().top - 50},
        500);
    }
    return false;
  });
  //
  // mobile nav
  //
  if($('.mobile-nav, .mobile-nav-button').length) {
    $('.mobile-nav-button').on('click', function(){
      $('.mobile-nav').toggleClass('active');
      $('html').toggleClass('disabled-scroll');
    });
  };
  //
  // browser navigator
  //
  var $browser_name = navigator.userAgent;
  if ($browser_name.toLowerCase().indexOf('edge') > -1) {
    $('html').addClass('edge').removeClass('chrome');
  }
  if ($browser_name.toLowerCase().indexOf('trident') > -1) {
    $('html').addClass('ie ie-11');
  }
  if ($browser_name.toLowerCase().indexOf('msie 10.0') > -1) {
    $('html').addClass('ie-10').removeClass('ie-11');
  }
  if ($browser_name.toLowerCase().indexOf('msie 9.0') > -1) {
    $('html').addClass('ie-9').removeClass('ie-11');
  };
  //
  // validate form
  //
  if($('.validate').length) {
    $.validator.setDefaults({
        errorPlacement: function() {}
    });
    $('form.validate').each(function() {
        $(this).validate();
    });
  } 
  //
  // map
  //
  if($('#map').length) {
    // map styles
      var styles_array = [
      {
        "featureType": "all",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "hue": "#6700ff"
            }
        ]
      },
      {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 65
            },
            {
                "visibility": "on"
            }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 51
            },
            {
                "visibility": "simplified"
            }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 30
            },
            {
                "visibility": "on"
            }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 40
            },
            {
                "visibility": "on"
            }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "hue": "#ffff00"
            },
            {
                "lightness": -25
            },
            {
                "saturation": -97
            }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [
          {
              "visibility": "on"
          },
          {
              "lightness": -25
          },
          {
              "saturation": -100
          }
        ]
      }
    ];
  // init map
    function initialize() {
      var lat = 52.4048965,
          lng = 16.9297491,
          image = 'assets/img/marker.png',
          map_canvas = document.getElementById('map'),
          map_options = {
            scrollwheel: false,
            zoom: 17,
            center: new google.maps.LatLng(lat, lng),
            mapTypeId: google.maps.MapTypeId.ROADMAP
          },
          map = new google.maps.Map(map_canvas, map_options),
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map,
            icon: image
          });

          map.setOptions({styles:styles_array});
    }
    google.maps.event.addDomListener(window, 'load', initialize);
  }


//
// sliders
//

// offer sliders
if($('.main-slider-container').length) {
    // offer sliders
    $('.offer-item__slider').each(function(){
        var $this = $(this);

        if($this.find('.main-slider-container').find('.offer-item__slider-main-item').length > 1) {
          // slider
          $this.find('.offer-item__slider-main').flickity({
            cellSelector: '.offer-item__slider-main-item',
            cellAlign: 'center',
            contain: true,
            pageDots: true,
            prevNextButtons: false,
            accessibility: true
          });
          // nav
          $this.find('.offer-item__slider-nav').flickity({
            asNavFor: $this.find('.offer-item__slider-main')[0],
            cellSelector: '.offer-item__slider-nav-item',
            cellAlign: 'center',
            contain: true,
            pageDots: false,
            prevNextButtons: false,
          });
        }
    });

  if($('.main-slider-container').find('.homepage-slider__item').length > 1) {
    // homepage slider
    $('.homepage-slider__items').flickity({
      cellSelector: '.homepage-slider__item',
      cellAlign: 'center',
      contain: true,
      pageDots: true,
      prevNextButtons: false,
      accessibility: true
    });
  }

    // prev/next buttons
    $('.slider-container').on( 'click', '.previous-arrow', function() {
      $(this).closest('.slider-container').flickity('previous');
    });
    $('.slider-container').on( 'click', '.next-arrow', function() {
      $(this).closest('.slider-container').flickity('next');
    });
}

//
// navs
//
if($('.main-header').length) {
  var $subnav = $('.subnav');

  // without subnav
  if(!$subnav.length) {
    var $main_header_height = $('.main-header').outerHeight();
    $('.main-header').siblings('main').css('margin-top', $main_header_height);

    $(window).resize(function(){
      var $main_header_height = $('.main-header').outerHeight();
      $('.main-header').siblings('main').css('margin-top', $main_header_height);
    });
  }
  // with subnav
  if($subnav.length) {
    var $subnav_height = $subnav.outerHeight();
    $subnav.siblings('main').css('margin-top', $subnav_height);

    // onepage nav
    $subnav.onePageNav({
      currentClass: 'active'
    });

    $(window).resize(function(){
      var $subnav_height = $subnav.outerHeight();
      $subnav.siblings('main').css('margin-top', $subnav_height);
    });
  }
  
  // on scroll
  if(!$subnav.length) {
    $(window).on('scroll', function(){
      var $scroll = $(this).scrollTop();

      if ($scroll > 0) {
        $('.main-header').addClass('main-header--thin');
        if($('.main-header').hasClass('main-header--white')) {
          $('.main-header').removeClass('main-header--white').addClass('temporary');
        }
      } else {
        $('.main-header').removeClass('main-header--thin');
        if($('.main-header').hasClass('temporary')) {
          $('.main-header').addClass('main-header--white').removeClass('temporary');
        }
      }
    });
  }
  if($subnav.length) {
    $('.main-header').addClass('static');
    var $subnav_offset = $subnav.offset().top;

     $(window).on('scroll', function(){
      var $scroll = $(this).scrollTop();

       if($scroll >= $subnav_offset) {
          if(!$subnav.hasClass('fixed')) {
            $subnav.addClass('fixed');
          }
       } if($scroll < $subnav_offset) {
          if($subnav.hasClass('fixed')) {
            $subnav.removeClass('fixed');
          }
       }
    });
  }
} // navs end
});
